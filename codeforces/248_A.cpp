#include <cstdio>

int main()
{
    int n;
    int l,r;
    int sum=0;
    scanf("%d",&n);
    int cnt_l=0,cnt_r=0;
    for (int i=0;i<n;i++)
    {
        scanf("%d%d",&l,&r);
        if (l == 1)
            cnt_l++;
        if (r == 1)
            cnt_r++;
    }
    if ( cnt_l > n/2 )
        sum=sum+(n-cnt_l);
    else
        sum=sum+cnt_l;
    if ( cnt_r > n/2 )
        sum=sum+(n-cnt_r);
    else
        sum=sum+cnt_r;
    printf("%d\n",sum);
    return 0;
}
