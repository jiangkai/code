#include <cstdio>
#include <cmath>
long long poww(int t){
    return (long long)pow(10,t);
}

int main()
{
    int n;
    long long int i;
    scanf("%d",&n);
    for (i=poww(n-1);i<=poww(n)-1;i++)
    {
        if ( i%2 == 0 && i%3 == 0 && i%5 == 0 && i%7 == 0  )
            break;
    }
    if (i == poww(n))
        printf("-1\n");
    else
        printf("%d\n",i);
}
