#include <cstdio>
#include <cmath>
#include <algorithm>

int lucky(int i){
    int cnt=0;
    while (i/10 != 0){
        if (i % 10 == 4 || i % 10 == 7){
            cnt++;
        }
        i/=10;
    }
    return (i % 10 == 4 || i % 10 == 7)?cnt+1:cnt;
}

int main(){
    int n,k;
    int t;
    int cnt=0;
    scanf("%d%d",&n,&k);
    for (int i=0;i<n;i++){
        scanf("%d",&t);
        if (lucky(t) <= k){
        	cnt++;
        }

    }
    printf("%d\n",cnt);
    return 0;
}