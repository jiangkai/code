#include <cstdio>
#include <cmath>
typedef long long LL;

int main()
{
    LL ab,bc,ca;
    int a,b,c;
    while (scanf("%I64d%I64d%I64d",&ab,&bc,&ca) != EOF)
    {
        LL abc=ab*bc*ca;
        a=sqrt(abc/bc/bc);
        b=sqrt(abc/ca/ca);
        c=sqrt(abc/ab/ab);
        printf("%d\n",(a+b+c)*4);
    }
    return 0;
}
