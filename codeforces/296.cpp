#include <cstdio>
int main(){
	int n;
	int a[200];
	int cn[2000]={0};
	scanf("%d",&n);
	for(int i=0;i<n;i++){
		scanf("%d",&a[i]);
		cn[a[i]]++;
	}
	int max=0;
	for(int i=1;i<=1000;i++){
		if(cn[i]>max)
			max=cn[i];
	}
	if(max<=(n/2)+(n%2))
		printf("YES\n");
	else
		printf("NO\n");
}
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
using namespace std;
int main(){
    int n;
    int a;
    int cn[1001]={0};
    scanf("%d",&n);
    for(int i=0;i<n;i++){
        scanf("%d",&a);
        cn[a]++;
    }
    sort(cn+1,cn+1000);
    int flag;
    for(int i=1;i<=1000;i++){
        if(cn[i]!=0){
            flag=i;
            break;
        }
    }
    int max=0;
    for(int i=flag;i<=1000;i++){
        if(cn[i]>max)
            max=cn[i];
    }
    int re=1;
    for(int i=flag;i<1000;i++){
        cn[i]=cn[i-1]+cn[i];
        if(cn[i+1]>cn[i]+1){
            re=0;
            break;
        }
    }


    if(max == n && n!=1){
        printf("NO\n");
        return 0;
    }
    if(n == 1 ){
        printf("YES\n");
    }
    else{
        if (re == 0)
            printf("NO\n");
        else
            printf("YES\n");
    }
}
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cctype>
using namespace std;

#define M 1000000007

long long a, b, c, d;
long long ans;
bool x, y, z;
int n;
char s[100010], w[100010];

int main() {
	int t;
	while (~scanf("%d", &n)) {
		scanf("%s", s);
		scanf("%s", w);
		a = b = c = d = 1;
		x = y = z = 0;
		for (int i = 0; i < n; i++) {
			if (isdigit(s[i]) && isdigit(w[i])) {
				if (s[i] < w[i])
					x = 1;
				else if (s[i] > w[i])
					y = 1;
			} else {
				z = 1;
				if (isdigit(s[i])) {
					t = s[i] - '0';
					a = (a * 10) % M;
					b = (b * (10 - t)) % M;
					c = (c * (t + 1)) % M;
				} else if (isdigit(w[i])) {
					t = w[i] - '0';
					a = (a * 10) % M;
					b = (b * (t + 1)) % M;
					c = (c * (10 - t)) % M;
				} else {
					a = (a * 100) % M;
					b = (b * 55) % M;
					c = (c * 55) % M;
					d = (d * 10) % M;
				}
			}
		}
		if (!z) {
			if (x && y)
				cout << "1\n";
			else
				cout << "0\n";
		} else {
			if (x && y)
				ans = a % M;
			else if (x)
				ans = (a - b) % M;
			else if (y)
				ans = (a - c) % M;
			else
				ans = (a - b - c + d) % M;
			while (ans < 0)
				ans += M;
			cout << ans << endl;
		}
	}
	return 0;
}