#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;

int main(){
    int n;
    long long a[300500];
    scanf("%d",&n);
    for(int i=0;i<n;i++){
        scanf("%I64d",&a[i]);
    }
    sort(a,a+n);
    long long cnt=0;
    for(int i=0;i<n;i++){
        cnt+=abs((i+1)-a[i]);
    }
    printf("%I64d\n",cnt);
}