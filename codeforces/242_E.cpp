#include<iostream>
#include<cstring>
using namespace std;
int tree[10005];

int lowbit(int x)
{
    return x&(-x);
}

void Modify(int a,int c,int n)
{
    while(a<=n)
    {
        tree[a]+=c;
        a+=lowbit(a);
    }
}

int Query(int a)
{
    int sum=0;
    while(a>0)
    {
        sum+=tree[a];
        a-=lowbit(a);
    }
    return sum;
}

int main()
{
    int cas_num,n,i,a,b;
    char order[10];
        memset(tree,0,sizeof(tree));
        cin>>n;
        for(i=1;i<=n;i++)
        {
            cin>>a;
            Modify(i,a,n);
        }
        int cnt,x;
        scanf("%d",&cnt);
        while(cnt--)
        {
            scanf("%s",order);
            if(order[0]=='1'){
            cin>>a>>b;
            printf("%d\n",Query(b)-Query(a-1));
            }
            if (order[0]=='2')
            {
                cin>>a>>b>>x;
                for (int i=a;i<=b;i++)
                    tree[i]=tree[i]^x;
            }
      }

    return 0;
}
