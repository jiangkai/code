#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#define MAXM 80
#define MAXN 30
using namespace std;
int parent[MAXN];
int n,m;
int sumweight;

struct edge
{
    int u,v;
    int weight;
}edges[MAXM];

void init()
{
    for (int i=1;i<=n;i++)
        parent[i]=-1;
}

int find(int x)
{
    int s;
    for (s=x;parent[s]>=0;s=parent[s]);
    while (s!=x)
    {
        int temp=parent[x];
        parent[x]=s;
        x=temp;
    }
    return s;
}

void Union(int R1,int R2)
{
    int r1=find(R1),r2=find(R2);
    int temp=parent[r1]+parent[r2];
    if (parent[r1]>parent[r2])
    {
        parent[r1]=r2;
        parent[r2]=temp;
    }
    else
    {
        parent[r2]=r1;
        parent[r1]=temp;
    }
}

int cmp(const void *a,const void *b)
{
    edge aa=*(const edge *) a;
    edge bb=*(const edge *) b;
    return aa.weight-bb.weight;
}

void kruskal()
{
    init();
    int number=0;
    sumweight=0;
    for (int i=0;i<m;i++)
    {
        if (find(edges[i].u)!=find(edges[i].v))
        {
            number++;
            sumweight+=edges[i].weight;
            Union(edges[i].u,edges[i].v);
        }
        if (number>=n-1)    break;
    }

}

int main()
{
    //freopen("in.txt","r",stdin);
    char c[3];
    int u,v,w;
    int num,sum;
    while (scanf("%d",&n) == 1)
    {
        if (n==0)   break;
        sum=0;
        int t=n-1;
        while (t--)
        {
            scanf("%s",&c);
            u=c[0]-'A'+1;
            scanf("%d",&num);
            while (num--)
            {
                scanf("%s",&c);
                v=c[0]-'A'+1;
                scanf("%d",&w);
                edges[sum].u=u;
                edges[sum].v=v;
                edges[sum].weight=w;
                sum++;
            }
        }
        m=sum;
        qsort(edges,sum,sizeof(edges[0]),cmp);
        kruskal();
        printf("%d\n",sumweight);
    }
}
