#include <cstdio>
#include <cstdlib>
#include <queue>
#define MAXN 11
#define MAXM 20
struct edge
{
    int u,v;
    int w;
}edges[MAXM];

int parent[MAXN];
int n,m;

void init()
{
    for (int i=1;i<=n;i++)
    {
        parent[i]=-1;
    }
}

int find(int x)
{
    int s;
    for (s=x;parent[s]>=0;s=parent[s]);
    while (s!=x)
    {
        int tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void Union(int R1,int R2)
{
    int r1=find(R1),r2=find(R2);
    int tmp=parent[r1]+parent[r2];
    if (parent[r1]>parent[r2])
    {
        parent[r1]=r2;
        parent[r2]=tmp;
    }
    else
    {
        parent[r2]=r1;
        parent[r1]=tmp;
    }
}

int cmp(const void *a,const void *b)
{
    edge aa=*(const edge*)a;
    edge bb=*(const edge*)b;
    return aa.w-bb.w;
}

void kruskal()
{
    int u,v;
    int sumweight=0;
    int num=0;
    init();
    for (int i=0;i<m;i++)
    {
        u=edges[i].u; v=edges[i].v;
        if (find(u)!=find(v))
        {
            printf("%d %d %d\n",u,v,edges[i].w);
            sumweight+=edges[i].w;
            num++;
            Union(u,v);
        }
        if (num>=n-1)   break;
    }
    printf("weight of MST is %d\n",sumweight);
}

int main()
{
    int u,v,w;
    freopen("MST.txt","r",stdin);
    scanf("%d%d",&n,&m);
    for (int i=0;i<m;i++)
    {
        scanf("%d%d%d",&u,&v,&w);
        edges[i].u=u;edges[i].v=v;edges[i].w=w;
    }
    qsort(edges,m,sizeof(edges[0]),cmp);
    kruskal();
    printf("%d",parent[4]);

    return 0;
}
