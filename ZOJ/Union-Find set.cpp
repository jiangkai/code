#include <cstdio>
int N;
int parent[1000];

void init()
{
    for (int i=0;i<N;i++)
        parent[i]=-1;
}

int find (int x)
{
    int s;
    for (s=x;parent[s]>=0;s=parent[s]);
    while (s!=x)
    {
        int tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void Union(int R1,int R2)
{
    int r1=find(R1),r2=find(R2);
    int tmp=parent[r1]+parent[r2];
    if (parent[r1]>parent[r2]) //parent都是负数，所以r1的结点数少于r2的
    {
        parent[r1]=r2;
        parent[r2]=tmp;
    }
    else
    {
        parent[r2]=r1;
        parent[r1]=tmp;
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    int x,y;
    scanf("%d",&N);
    init();
    while (scanf("%d%d",&x,&y)==2)
    {
        if (x==0 && y==0)   break;
        if (find(x)!= find(y))
        {
            Union(x,y);
        }
    }
    printf("%d",parent[3]);
}
