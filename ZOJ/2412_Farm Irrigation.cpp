#include <cstdio>
#include <cstring>

char lei[52][52];
int map[160][160];
int A[33][3]={{0,1,0},{1,1,0},{0,0,0},{0,1,0},{0,1,1},{0,0,0},{0,0,0},{1,1,0},{0,1,0},{0,0,0},{0,1,1},{0,1,0},{0,1,0},{0,1,0},{0,1,0},{0,0,0},{1,1,1},{0,0,0},{0,1,0},{1,1,1},{0,0,0},{0,1,0},{1,1,0},{0,1,0},{0,0,0},{1,1,1},{0,1,0},{0,1,0},{0,1,1},{0,1,0},{0,1,0},{1,1,1},{0,1,0}};
int count,M,N;
int dir[4][2]={{0,1},{0,-1},{1,0},{-1,0}};

void dfs(int x,int y)
{
    int i,xx,yy;
    map[x][y]=0;
    for (i = 0;i<4;i++)
    {
        xx=x+dir[i][0];
        yy=y+dir[i][1];
        if (xx<0 || yy<0 || xx>=M*3 || yy>=N*3) continue;
        if ( map[xx][yy] == 1 )
            dfs(xx,yy);
    }
}

int main()
{
    //freopen("in.txt","r",stdin);
    while (scanf("%d%d",&M,&N)==2)
    {
        count=0;
        memset(map,0,sizeof(map));
        if ( M<=0 && N<=0 )   break;
        for (int i=0;i<M;i++)
        {
            scanf("%s",lei[i]);
            for (int j=0;j<N;j++)
            {
                for (int k=0;k<3;k++)
                    for (int x=0;x<3;x++)
                        map[i*3+k][j*3+x]=A[(lei[i][j]-'A')*3+k][x];
            }
        }
        for (int i=0;i<M*3;i++)
        {
            for (int j=0;j<N*3;j++)
            {
                if (map[i][j] == 1)
                {
                    dfs(i,j);
                    count++;
                }
            }
        }
        printf("%d\n",count);
    }
    return 0;
}
