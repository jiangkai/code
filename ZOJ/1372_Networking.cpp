#include <cstdio>
#include <cstring>
#define MAXN 51
const long INF=100000;

int n,m;
int edge[MAXN][MAXN];
int nearvex[MAXN];
int lowcost[MAXN];

void prim(int u0)
{
    int sumweight=0;
    for (int i=1;i<=n;i++)
    {
        lowcost[i]=edge[u0][i];
        nearvex[i]=u0;
    }
    nearvex[u0]=-1;
    for (int i=1;i<n;i++)
    {
        int min=INF;
        int v=-1;
        for (int j=1;j<=n;j++)
        {
            if (nearvex[j] != -1 && lowcost[j] < min)
            {
                v=j;
                min=lowcost[j];
            }
        }
        if ( v != -1)
        {
            nearvex[v]=-1;
            sumweight+=lowcost[v];
            for (int j=1;j<=n;j++)
            {
                if (nearvex[j] != -1 && edge[v][j]<lowcost[j])
                {
                    lowcost[j]=edge[v][j];
                    nearvex[j]=v;
                }
            }
        }
    }
    printf("%d\n",sumweight);
}

int main()
{
    //freopen("in.txt","r",stdin);
    int u,v,w;
    while (scanf("%d",&n) == 1)
    {
        if (n == 0)     break;
        scanf("%d",&m);
        for (int i=1;i<=n;i++)
            for (int j=1;j<=n;j++)
            {
                if (i==j)
                    edge[i][j]=0;
                else
                    edge[i][j]=INF;
            }
        for(int i=1;i<=m;i++)
        {
            scanf("%d%d%d",&u,&v,&w);
            if (edge[u][v] != 0 && edge[u][v]>w)
                edge[u][v]=edge[v][u]=w;
            else if (edge[u][v] == 0)
                edge[u][v]=edge[v][u]=w;
        }
        prim(1);
    }
    return 0;
}
