#include <cstdio>
#include <algorithm>
#include <cstring>
#define maxn 50
#define maxm 1000

struct exchange
{
    int ci,cj;
    double cij;
}ex[maxm];

int n,m;
int flag;
double maxdist[maxm];
char money[maxn][100],a[100],b[100];
int kase=1;
double x;

int readcase()
{
    int i;
    scanf("%d",&n);
    if ( n == 0 )   return 0;
    for (i=0;i<n;scanf("%s",money[i++]));
    scanf("%d",&m);
    for (i=0;i<m;i++)
    {
        char a[100],b[100];
        double x;
        int j,k;
        scanf("%s%lf%s",a,&x,b);
        for ( j=0;strcmp(a,money[j]);j++);
        for ( k=0;strcmp(b,money[k]);k++);   //!!!
        ex[i].ci=j;
        ex[i].cj=k;
        ex[i].cij=x;
    }
    return 1;
}

void bellmanford(int v0)
{
    flag=0;
    memset(maxdist,0,sizeof(maxdist));
    maxdist[v0]=1.0;
    for (int k=1;k<=n;k++)
    {
        for (int i=0;i<m;i++)
        {
            if ( maxdist[ex[i].ci]*ex[i].cij > maxdist[ex[i].cj])
            {
                maxdist[ex[i].cj] = maxdist[ex[i].ci]*ex[i].cij;
            }
        }
    }
    if ( maxdist[v0] > 1.0) flag=1;
}

int main()
{
    freopen("in.txt","r",stdin);
    while (readcase())
    {
        for (int i=0;i<n;i++)
        {
            bellmanford(i);
            if (flag)   break;
        }
        if (flag)
            printf("Case %d: Yes\n",kase++);
        else
            printf("Case %d: No\n",kase++);
    }
    return 0;
}
