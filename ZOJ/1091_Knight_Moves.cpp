#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;

char wei[6];
int sx,sy;
int tx,ty;
int sum;
int vis[8][8];
int dir[8][2]={{2,1},{-2,1},{-2,-1},{2,-1},{1,2},{1,-2},{-1,-2},{-1,2}};

struct Point
{
    int x,y;
    int step;
};
queue<Point> q;

int bfs(Point s)
{
    int xx,yy;
    vis[s.x][s.y]=1;
    q.push(s);
    Point tmp;
    Point hd;
    while ( !q.empty() )
    {
        hd=q.front();
        q.pop();
        if (hd.x == tx && hd.y == ty)
        {
            sum=hd.step;
            break;
        }
        for (int i=0;i<8;i++)
        {
            xx=hd.x+dir[i][0],yy=hd.y+dir[i][1];
            if ( xx>=0 && xx<8 && yy>=0 && yy<8 && !vis[xx][yy] )
            {
                vis[xx][yy]=1;
                tmp.x=xx;
                tmp.y=yy;
                tmp.step=hd.step+1;
                q.push(tmp);
            }
        }
    }
    return sum;
}

int main()
{
    //freopen("in.txt","r",stdin);
    while ( gets(wei) )
    {
        sx=wei[0]-'a';
        sy=wei[1]-'1';
        tx=wei[3]-'a';
        ty=wei[4]-'1';
        memset(vis,0,sizeof(vis));
        while(!q.empty())
        {
          q.pop();
        }                  //clear the queue !!!!!!!!!!!!
        Point start;
        start.x=sx;
        start.y=sy;
        start.step=0;
        printf("To get from %c%c to %c%c takes %d knight moves.\n",wei[0],wei[1],wei[3],wei[4],bfs(start));
    }
    return 0;
}
