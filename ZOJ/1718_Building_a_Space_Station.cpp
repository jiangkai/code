#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <functional>
#include <cmath>
#define MAXN 105
#define MAXM 6000
using namespace std;

int n,m;
double sum;
int parent[MAXN];
struct point
{
    double x,y,z;
    double rad;
} p[MAXN];

struct edge
{
    int u,v;
    double w;
} edges[MAXM];

double length(point p1,point p2)
{
    double tmp=0;
    tmp=(p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z);
    return sqrt(tmp);
}

int cmp(const void *a,const void *b)
{
    edge aa=*(const edge *)a;
    edge bb=*(const edge *)b;
    return aa.w>bb.w;
}

void uset()
{
    for (int i=0; i<n; i++)
    {
        parent[i]=-1;
    }
}

int find (int x)
{
    int s;
    for (s=x; parent[s]>=0; s=parent[s]);
    while (s != x)
    {
        int tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void Union(int u,int v)
{
    int r1=find(u),r2=find(v);
    int tmp=parent[r1]+parent[r2];
    if (parent[r1]<parent[r2])
    {
        parent[r1]=tmp;
        parent[r2]=r1;
    }
    else
    {
        parent[r2]=tmp;
        parent[r1]=r2;
    }

}

void kruskal()
{
    int num=0;
    int u,v;
    uset();
    for (int i=0; i<m; i++)
    {
        u=edges[i].u;
        v=edges[i].v;
        if (find(u)!=find(v))
        {
            sum+=edges[i].w;
            num++;
            Union(u,v);
        }
        if ( num >= n-1 )   break;
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    while (scanf("%d",&n) == 1)
    {
        if (n == 0)   break;
        point p[MAXM];
        for (int i=0; i<n; i++)
            scanf("%lf%lf%lf%lf",&p[i].x,&p[i].y,&p[i].z,&p[i].rad);
        int mi=0;
        for (int i=0; i<n; i++)
            for (int j=i+1; j<n; j++)
            {
                double tmp;
                tmp=length(p[i],p[j]);
                edges[mi].u=i;
                edges[mi].v=j;
                if ( (tmp-p[i].rad-p[j].rad) < 0)
                    edges[mi++].w = 0;
                else
                    edges[mi++].w = tmp-p[i].rad-p[j].rad;
            }
        m=mi;
        qsort(edges,m,sizeof(edges[0]),cmp);
        sum=0;
        kruskal();
        printf("%.3lf\n",sum);
    }
    return 0;
}
