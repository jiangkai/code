#include <cstdio>
#include <cstring>
#define MAXN 2000
#define INF 100000
#define codelen 7
int N;
char codes[MAXN][codelen+2];
int d[MAXN][MAXN];
int lowcost[MAXN];

int min_tree()
{
    int dist;
    int j;
    memset(d,0,sizeof(d));
    for (int i=0;i<N;i++)
    {
        for(j=i+1;j<N;j++)
        {
            dist=0;
            for (int k=0;k<7;k++)
            {
                dist+=codes[i][k]!=codes[j][k];
            }
            d[i][j]=d[j][i]=dist;
        }
    }
    int sum=0;
    lowcost[0]=-1;        //start from node 0
    for (int i=1;i<N;i++)
        lowcost[i]=d[0][i];
    for (int i=1;i<N;i++)
    {
        int min=INF;
        for (int k=0;k<N;k++)
        {
            if (lowcost[k] != -1 && lowcost[k] < min)
            {
                j=k;
                min=lowcost[k];
            }
        }
        sum+=min;
        lowcost[j]=-1;
        for (int k=0;k<N;k++)
        {
            if (d[j][k]<lowcost[k])
                lowcost[k]=d[j][k];
        }
    }
    return sum;
}

int main()
{
    while (1)
    {
        scanf("%d",&N);
        if (N == 0)     break;
        for (int i=0;i<N;i++)
        {
            scanf("%s",codes[i]);
        }
        printf("The highest possible quality is 1/%d.\n",min_tree());
    }
    return 0;
}
