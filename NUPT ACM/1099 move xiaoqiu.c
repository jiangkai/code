#include <stdio.h>
#define maxn 500010
int left[maxn],right[maxn];
void link(int X,int Y)
{
    right[X]=Y;
    left[Y]=X;
}

int main ()
{
    int X,Y,i,m,flag=1,p;
    long n;
    char type[3];
    scanf("%d %d",&n,&m);
    for (i=1;i<=n;i++)
    { left[i]=i-1;right[i]=i+1; }
    right[0]=1;
    while(m--)
    {
        scanf("%s%d%d",type,&X,&Y);
        link(left[X],right[X]);
        if (type[0]=='A')
        {
            link(left[Y],X);
            link(X,Y);
        }
        else
        {
            link(X,right[Y]);
            link(Y,X);
        }
    }
    p=right[0];
    while (n--)
    //while(p!=n+1)
    {
        if (flag == 1)  {printf("%d",p);flag=0;}
        else {printf(" %d",p);}
        p=right[p];
    }
    printf("\n");
    return 0;
}
