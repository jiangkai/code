#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

int n,L;
int a[10010];

int cmp(const void *_a,const void *_b)
{
    int *a=(int*)_a;
    int *b=(int*)_b;
    return *a-*b;
}

int main()
{
    freopen("in.txt","r",stdin);
    scanf("%d",&n);
    scanf("%d",&L);
    for (int i=0;i<n;i++)
        scanf("%d",&a[i]);
    qsort(a,n,sizeof(int),cmp);
    int sum=0;
    /*int front=0,rear=n-1,

    while ( front<=rear )
    {
        if ( a[front]+a[rear] <= L && front!=rear)
        {
            sum++;
            front++;
            rear--;

        }
        if (a[front]+a[rear] > L  && front!=rear)
        {
            sum++;
            rear--;
        }
        if ( front == rear )
        {
            sum++;
            front++;
            rear--;
        }
    }*/
    for(int i=0,j=n-1;i<=j;)
    {
        if(a[i]+a[j]<=L && i!=j)
        {
            sum++;
            i++;
            j--;
        }
        if(a[i]+a[j]>L&&i!=j)
        {
            sum++;
            j--;
        }
        if(i==j)
        {
            sum++;
            i++;
            j--;
        }
    }
    printf("%d\n",sum);
    return 0;
}
