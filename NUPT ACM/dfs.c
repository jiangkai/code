#include<stdio.h>
#define max 705

int Matrix[max][max];

void Search(int m,int n)
{
    if(Matrix[m][n] == 0)
        return ;
    Matrix[m][n] = 0;

    Search(m-1,n-1);Search(m,n-1);Search(m+1,n-1);
    Search(m-1,n);Search(m+1,n);
    Search(m-1,n+1);Search(m,n+1);Search(m+1,n+1);
}

int main()
{
    int i,j,n;
    int count = 0;
    char ch;

    for(i=0;i<max;i++)
        for(j=0;j<max;j++)
            Matrix[i][j] = 0;

    scanf("%d",&n);
    getchar();

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            scanf("%c",&ch);
            Matrix[i][j] = ch - 48;
        }
        getchar();
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            if(Matrix[i][j])
            {
                count ++;
                Search(i,j);
            }
        }
    }
    printf("%d\n",count);

    return 0;
}
