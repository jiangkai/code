#include <cstdio>
int GCD(int a,int b);
int LCM(int a,int b);
int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
	int num1,num2,gcd,lcm;
	while(scanf("%d%d",&num1,&num2)!=EOF){
		gcd=GCD(num1,num2);
		lcm=LCM(num1,num2);
		printf("%d %d\n",gcd,lcm);
	}
}
int GCD(int a,int b)
{
	if ( a % b == 0)
	{
        return b;
	}
	else
	return GCD ( b,a % b) ;
}
int LCM(int a,int b)
{
	int temp_lcm;
	temp_lcm=a*b/GCD(a,b);
	return temp_lcm;
}
