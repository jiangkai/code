#include <stdio.h>

int main()
{
    int n,s,i,j;
    scanf("%d",&n);
    while (n--)
    {
        scanf("%d",&s);
        for (i=2;i<s;i++)
            if ( s%i == 0) break;
        if (i<s||s<=0)
            printf("No\n");
        else
            printf("Yes\n");
    }
    return 0;
}
