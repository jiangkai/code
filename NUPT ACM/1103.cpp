#include <iostream>
#include <algorithm>
using namespace std;
int const  MAXN=10;
int main()
{
    int n,i,p[MAXN];
    cin>>n;
    for (i = 0; i<n; i++)
        cin>>p[i];
    sort(p, p+n);
    do {
        for (i=0; i<n; i++)
            if (i)
                printf(" %d", p[i]);
            else
                printf("%d", p[i]);
        printf("\n");
    } while (next_permutation(p, p+n));
    return 0;
 }
