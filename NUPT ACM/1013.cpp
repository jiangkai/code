#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

double ep=1e-4;

double max(double a,double b){
    return a>b ? a : b ;
}
double min(double a,double b){
    return a>b ? b : b ;
}

double Min(double a,double b,double c){
    return min(min(a,b),c);
}
double Max(double a,double b,double c){
    return max(max(a,b),c);
}

int main(){
    double a,b,c;
    while(cin>>a>>b>>c){
        double maxs=Max(a,b,c),mins=Min(a,b,c),sum=a+b+c;
        if ((a-0)+(b-0)+(c-0) < ep)
            break;
        if ( 2*maxs - sum >= 0)
            printf("Not a triangle\n");
        else{
            if (abs(a-b)<ep || abs( b-c )<ep  || abs( a-c)<ep){
                if( abs(maxs*maxs - 2*mins*mins) <ep)
                    printf("Isosceles right triangle\n");
                else{
                    if (a==b && b==c)
                        printf("Equilateral triangle\n");
                    else
                        printf("Isosceles triangle\n");
                }
            }
            else{
                if (abs(maxs*maxs - 2*mins*mins) <ep )
                    printf("Right triangle\n");
                else
                    printf("General triangle\n");
            }
        }
    }
    
    cout<<"End\n";
}