#include <stdio.h>
#include <string.h>
#define MAXN 10

int ten(int n)
{
    int i;
    int count=1;
    if (n==0) return count;
    else {
        for (i=0;i<n;i++) count*=10;
    return count;
    }
}

int main()
{
    char num[MAXN];
    int count=0;
    int i=0,n=0,sum=0;
    scanf("%s",num);
    int length=strlen(num);
    if (length==1)
    count=num[0]-'0';
    else
    {   count+=((num[0]-'0')-1)*length*ten(length);
        for (i=length-1;i>=1;i--)
        {
            sum+=(num[i]-'0')*ten(n);
            n++;
        }
        sum++;
        count+=(sum*length);
        for (i=1;i<=length-1;i++)
        {
            count+=(ten(i-1)*9)*i;
        }
    }
    printf("%d\n",count);
    return 0;
}
