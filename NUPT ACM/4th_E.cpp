#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

int vis[255][255];
char board[255][255];
int r,c;
int dx[2]={-1,1},dy[2]={-1,1};
int wolf[255],sheep[255];
int wolfsum=0,sheepsum=0;
int sum=0;

int count(int x,int y)
{
    if ( board[x][y] == 'v' )
        wolf[sum]++;
    if ( board[x][y] == 'o' )
        sheep[sum]++;
}

void dfs(int x,int y)
{
    if ( vis[x][y] || board[x][y] == '#'  || x == 1 || x == r || y == 1 || y == c )
        return;
    vis[x][y]++;
    if ( board[x][y] == 'v' || board[x][y] == 'o' )
        count(x,y);
    if ( board[x][y] == '.' || board[x][y] == 'v' || board[x][y] == 'o')
    {
        dfs(x+dx[0],y);dfs(x+dx[1],y);
        dfs(x,y+dy[0]);dfs(x,y+dy[1]);
    }
}

void dfs_jie(int x,int y)
{
    if ( vis[x][y] || board[x][y] == '#' || x == 0 || x == r+1 || y == 0 || y == c+1 )
        return;
     vis[x][y]++;
    if (board[x][y] == '.' || board[x][y] == 'v' || board[x][y] == 'o')
    {
        dfs(x+dx[0],y);dfs(x+dx[1],y);
        dfs(x,y+dy[0]);dfs(x,y+dy[1]);
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    for( int i=0;i<=r+1;i++)
    {
        board[0][i]='.';
        board[c][i]='.';
    }
    for(int j=0;j<=c+1;j++)
    {
        board[j][0]='.';
        board[j][c]='.';
    }
    memset(vis,0,sizeof(vis));
    memset(sheep,0,sizeof(sheep));
    memset(wolf,0,sizeof(wolf));
    scanf("%d %d",&r,&c);
    for (int i=1;i<=r;i++)
        scanf("%s",&board[i][1]);
    for( int i=1;i<=c;i++)
    {
        dfs_jie(1,i);
        dfs_jie(c,i);
    }
    for(int j=1;j<=r;j++)
    {
        dfs_jie(j,1);
        dfs_jie(j,c);
    }
    for (int i=0;i<=r+1;i++)
        for (int j=0;j<=c+1;j++)
        {
            if ( !vis[i][j] )
            {
                dfs(i,j);
                sum++;
            }
        }
    int lan=sum;
    for (int i=0;i<lan;i++)
    {
        if ( sheep[i]>wolf[i] )
            sheepsum += sheep[i];
        else
            wolfsum += wolf[i];
    }
    printf("%d %d",sheepsum,wolfsum);
    return 0;
}
