#include <iostream>
#include <cstdlib>
#include <cstring>
#define MAXN 10010

char a[MAXN];

int main()
{
    int sum=0;
    scanf("%s",a);
    int len=(int)strlen(a);
    for(int i=0;i<len;i++)
        sum+=a[i]-'0';
    printf("%d",sum);
    return 0;
}
