#include <iostream>
#include <cstdio>
#include <cstdlib>

int ip[9];
int a[4];

void change(int a)
{
    int i=7,j,k,s;
    while ( a!=0 )
    {
        s=a%2;
        ip[i--] = s;
        a=a/2;
    }
    for (j=0;j<8;j++)
        if(ip[j]) break;
    for (k=j;k<8;k++)
        printf("%d",ip[k]);
}

int main()
{
    int flag=1;
    scanf("%d.%d.%d.%d",a,a+1,a+2,a+3);
    for (int i=0;i<4;i++){
        memset(ip,0,sizeof(ip));
        if (flag){
            change(a[i]);
            flag=0;
        }
        else {
            printf(".");
            change(a[i]);
        }
    }
    printf("\n");
    return 0;
}
