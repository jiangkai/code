#include <stdio.h>
#include <time.h>
#include <stdlib.h>
typedef struct card
{
    char color;
    int num;
}ka;

void display (const ka* p,int n );
void shuffle (ka* p,int n );
int main()
{
    int i;
    ka deck[52];
    for(i=0;i<52;i++)
    {
        deck[i].color=i/13+3;
        deck[i].num=i%13+1;
    }
    printf("before shuffle:\n");
    display(deck,52);
    shuffle(deck,52);
    printf("after n shuffle:\n");
    display(deck,52);
    return 0;
}

void display (const ka* p,int n )
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("(%c%-2d) ",p[i].color,p[i].num);
        if ((i+1)%13==0)
        {
            printf("\n");
        }
    }
}

void shuffle (ka* p,int n )
{
    int i,j;
    ka temp;
    srand(time(0));
    for(i=0;i<n;i++)
    {
        j=rand()%n;
        temp=p[i];
        p[i]=p[j];
        p[j]=temp;
    }
}
