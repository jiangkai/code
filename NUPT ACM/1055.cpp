#include <iostream>
int n,m;

void win(int m)
{
    int count=1;
    while (m > 1)
    {
        if (m % 2 == 1)
        {
            m= m*3 + 1;
            count++;
        }
        else
        {
            m/=2;
            count++;
        }
    }
    if (count % 2 == 0 || count==1)
        printf("I win!\n");
    else
        printf("I lost!\n");
}

int main()
{
    scanf("%d",&n);
    while ( n-- )
    {
        scanf("%d",&m);
        win(m);
    }
    return 0;
}
