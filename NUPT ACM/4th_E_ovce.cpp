#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

int vis[255][255];
char board[255][255];
int r,c;
int dx[2]={-1,1},dy[2]={-1,1};
int wolf[255],sheep[255],flag[255];
int wolfsum=0,sheepsum=0;
int sum=0;

int count(int x,int y)
{
    if ( board[x][y] == 'v' )
        wolf[sum]++;
    if ( board[x][y] == 'o' )
        sheep[sum]++;
}

void dfs(int x,int y)
{
    if ( vis[x][y] || board[x][y] == '#' )
    {
        if ( x == 0 || x == r-1 || y == 0 || y == c-1 )
            {
                flag[sum] =1;
                return;
            }
        else
        return;
    }
    vis[x][y]=1;
    if ( board[x][y] == 'v' || board[x][y] == 'o' )
        count(x,y);
    if ( board[x][y] == '.' || board[x][y] == 'v' || board[x][y] == 'o')
    {
        dfs(x+dx[0],y);dfs(x+dx[1],y);
        dfs(x,y+dy[0]);dfs(x,y+dy[1]);
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    memset(vis,0,sizeof(vis));
    memset(sheep,0,sizeof(sheep));
    memset(wolf,0,sizeof(wolf));
    memset(flag,0,sizeof(flag));
    scanf("%d %d",&r,&c);
    for (int i=0;i<r;i++)
        scanf("%s",board[i]);
    for (int i=0;i<r;i++)
        for (int j=0;j<c;j++)
        {
            if ( !vis[i][j] )
            {
                dfs(i,j);
                ++sum;
            }
        }
    int lan=sum;
    for (int i=0;i<lan;i++)
    {
        if (flag[i])
            continue;
        if ( sheep[i]>wolf[i] )
            sheepsum += sheep[i];
        else
            wolfsum += wolf[i];
    }
    printf("%d %d",sheepsum,wolfsum);
    return 0;
}
