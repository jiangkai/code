/*#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
using namespace std;

int mark[2001];
int gcd(int a, int b)
{
    return b ? gcd(b, a % b) : a;
}
int min(int a, int b)
{
    return a < b ? a : b;
}
int max(int a, int b)
{
    return a > b ? a : b;
}
int main()
{
    int n;
    int r, s, z, x, y;
    int cnt, num;
    int flag;
    int i;
    while (scanf("%d", &n) != EOF)
    {
        memset(mark, 0, sizeof(mark));
        cnt = 0;
        flag = 0;
        num = 0;
        for (r = 1; r * r <= n; r++)
        {
            for (s = 1; s < r; s++)
            {
                z = r * r + s * s;
                if (z <= n)
                {
                    x = min(r * r - s * s, 2 * r * s);
                    y = max(r * r - s * s, 2 * r * s);
                    flag = 0;
                    if (gcd(x, y) > 1) flag++;
                    if (gcd(y, z) > 1) flag++;
                    if (gcd(x, z) > 1) flag++;
                    if (flag == 0)
                    {
                        cnt++;
                        for (i = 1; i * z <= n; i++)
                        {
                            mark[i * x] = 1;
                            mark[i * y] = 1;
                            mark[i * z] = 1;
                        }
                    }
                }
            }
        }
        for (i = 1; i <= n; i++)
        {
            if (mark[i] != 1) num++;
        }
        printf("%d %d\n",cnt,num);
    }
    return 0;
}*/
#include<iostream>
#include<stdio.h>
#include<math.h>
using namespace std;
int a[1000001];
int gcd(int x,int y)
{
    if (y==0) return x;
    return gcd(y,x%y);
}

int main()
{
    int i,j,tot,tt,x,y,z,k,n;
    while(scanf("%d",&n)!=EOF)
    {
        for(i=1; i<=n; i++)
            a[i]=0;
        tot=0;
        for(i=1; i<=int(sqrt(n/2.0)); i++)
            for(j=i+1; j<=int(sqrt(n+0.0)); j++)
            {
                x=j*j-i*i;
                y=2*j*i;
                z=i*i+j*j;
                if(z>n) break;
                if(gcd(x,y)==1)
                {
                    tot++;
                for (k=1; k<=n/z; k++)
                        a[x*k]=a[y*k]=a[z*k]=1;
                }
            }
        tt=0;
        for(i=1; i<=n; i++)
            if (a[i]==0)
                tt++;
        printf("%d %d\n",tot,tt);
    }
    return 0;
}
