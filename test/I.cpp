#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<algorithm>
using namespace std;
const double eps =1e-9;
const double PI = acos(-1);
struct point {
    double x,y;
}a[1000];
double b[2000];
__int64 C(int a,int b)
{
    int i;
    __int64 aa=a,bb=b,ans=1;
    for(i=0;i<b;i++) ans*=(aa-i);
    for(i=2;i<=b;i++)ans/=i;
    return ans;
}
int main()
{freopen("in.txt","r",stdin);
    int t,cases=1;
    int n;
    int i,j;
    __int64 ans,count;
    scanf("%d",&t);
    while(t--)
    {
        ans=0;
        scanf("%d",&n);
        for(i=0;i<n;i++) scanf("%lf%lf",&a[i].x,&a[i].y);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                if(i==j) continue;
                double tmp=atan2(a[j].y-a[i].y,a[j].x-a[i].x);
                if(tmp<-eps) tmp+=2*PI;
                if(j<i) b[j]=tmp;
                else b[j-1]=tmp;
            }
            sort(b,b+n-1);
            for(j=0;j<n-1;j++) b[n-1+j]=b[j]+2*PI;
            int k=0;
            count=0;
            for(j=0;j<n-1;j++)
            {
                while(fabs(b[k]-b[j])<PI) k++;
                if(k-j-1>=2) count+=C(k-j-1,2);
            }
            ans+=C(n-1,3)-count;
        }
        printf("%I64d\n",C(n,4)-ans);
    }
    return 0;
}
