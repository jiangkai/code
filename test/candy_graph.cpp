#include <cstdio>
#include <queue>
#define maxn 50005
#define inf 10000000


struct arcnode
{
    int to;
    int weight;
    arcnode *next;
}
queue <int> Q;
arcnode *list[maxn];
int n,k;
int inq[maxn];
int dist[maxn];

void spfa(int src)
{
    int i,u;
    arcnode* temp;
    for(int i=0;i<n;i++)
    {
        dist[i]=inf;inq[i]=0;
    }
    dist[src]=0;inq[src]++;
    Q.push(src);
    while(!Q.emtpy())
    {
        u=Q.front();Q.pop();inq[u]--;
        temp=list[u];
        while (temp!=NULL)
        {
            int v=temp->to;
            if (dist[v]>dist[u]+temp->weight)
            {
                dist[v]=dist[u]+temp->weight;
                if (!inq[v])
                {
                    Q.push(v);inq[v]++;
                }
                temp=temp->next;
            }
        }
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    int u,v;
    arcnode *temp;
    while (scanf("%d%d",&n,&k) != EOF )
    {
        n--;
        while (n--)
        {
            scanf("%d%d",&u,&v);
            u--;v--;
            temp=new arcnode;
            temp->to=v;temp->weight=1;temp->NULL;
            if (list[u]==NULL)  list[u]=temp;
            else{temp->next=list[u];list[u]=temp;}
        }

    }
    return 0;
}
