#include <cstdio>
#include <iostream>
#include <cstring>
#include <stack>
#include <vector>
#define maxn 1005
using namespace std;

int a[1005];
int main()
{
    int kase;
    int i;
    vector<int> ve;
    while (scanf("%d",&kase) != EOF)
    {
        while(!ve.empty())
        {
            ve.pop_back();
        }
        for (int i=0; i<kase; i++)
        {
            scanf("%d",&a[i]);
            ve.push_back(a[i]);
        }
        if (kase % 2 == 1)
        {
            printf("0\n");
            continue;
        }
        vector <int>::iterator it;
        vector <int>::iterator top;
        int flag=1;
        int t=kase/2;
        while ( t-- )
        {
            top=ve.begin();
            for (it=ve.begin()+1,i=1; i<6 && it!=ve.end(); it++,i++)
            {
                //printf("|%d|\n",*top);
                if (*top == *it)
                {
                    ve.erase(top);
                    ve.erase(it-1);
                    break;
                }
            }
        }
        printf("%d\n", ve.empty() ? 1 : 0 );
    }
    return 0;
}

