#include <cstdio>
#include <cstring>

char num[17];
int plus[15];
int number[2]={0,1};
int cnt;

void solve()
{
    int len=strlen(num);
    int sum=0;
    int dec=1;
    for (int i=len-1;i>=0;i--)
    {
        if ( plus[i] == 1 )
        {
            dec=1;
            sum+=(num[i]-'0')*dec;

        }
        else
        {
            sum+=(num[i]-'0')*dec;
            dec*=10;
        }
    }
    int sum2=sum/2;
    sum=0;
    dec=1;
    for (int i=len-1;i>=0;i--)
    {
        if (plus[i] == 1)
        {

            sum+=(num[i]-'0')*dec;
            dec=1;
        }
        else
        {
            sum+=(num[i]-'0')*dec;
            dec*=10;
        }
        if ( sum == sum2)
            cnt++;
        if (sum > sum2)
            break;
    }

}

void combine(int n, int m)
{
     for(int i=n;i>0;i--)
     {
         plus[m]=number[i-1];
         if (m>1)
            combine(n,m-1);
         else
         {
            solve();
         }
     }
}

int main()
{
    while (scanf("%s",&num))
    {
        cnt=0;
        int len=strlen(num);
        if (num[0] == 'E' && num[1] == 'N' && num[2] == 'D')
            break;
        for(int i=0;i<len-1;i++)
            plus[i]=0;
        combine(2,len-1);
        printf("%d\n",cnt);
    }
    return 0;
}
