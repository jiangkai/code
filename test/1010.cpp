#include <cstdio>
#include <cstring>
#include <cmath>

char dict[5050][8];
int phone[5050];
int var[999999];
int result[5050];

int zhuan(char a)
{
    int tmp;
    if (a == 'a' || a == 'b' || a == 'c')
        tmp=2;
    if ( a == 'd' || a == 'e' || a == 'f')
        tmp=3;
    if ( a == 'g' ||a == 'h' ||a == 'i' )
        tmp=4;
    if ( a == 'j' ||a == 'k' ||a == 'l' )
        tmp=5;
    if ( a == 'm' ||a == 'n' ||a == 'o' )
        tmp=6;
    if ( a == 'p' ||a == 'q' ||a == 'r' || a == 's')
        tmp=7;
    if ( a == 't' ||a == 'u' ||a == 'v' )
        tmp=8;
    if ( a == 'w' ||a == 'x' ||a == 'y' || a == 'z')
        tmp=9;

    return tmp;
}

int main()
{
    freopen("in.txt","r",stdin);
    int kase;
    int i;
    int dictge,phonege;
    while ( ~scanf("%d",&kase) )
    {
        memset(var,0,sizeof(var));
        scanf("%d%d",&phonege,&dictge);
        for(i=0;i<phonege;i++)
        {
            scanf("%d",&phone[i]);
        }

        for (i=0;i<dictge;i++)
        {
            scanf("%s",dict[i]);
            int len=strlen(dict[i]);
            int shu=0;
            int dec2=1;
            for (int j=len-1;j>=0;j--)
            {
                shu+=zhuan(dict[i][j])*dec2;
                dec2*=10;
            }
            var[shu]++;
        }

        for (int i=0;i<phonege;i++)
        {
            printf("%d\n",var[phone[i]]);
        }
    }
    return 0;
}
