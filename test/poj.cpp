#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <iostream>
using namespace std;

int a[1500];

int main(){
    memset(a,0,sizeof(a));
	int n,tmp;
	scanf("%d",&n);
	tmp=n;
	int h,m;
	int time;
	while (n--){
		scanf("%d %d",&h,&m);
		time=h*60+m;
		a[time]++;
	}
	int max=-1;
	for(int i=0;i<1440;i++)
	{
	    if (a[i]>max)
            max=a[i];
	}
	cout<<max<<endl;
	return 0;
}
