#include <cstdio>
#include <cstring>
int a[100005];
const double eps=1e-10;
int main()
{
    freopen("in.txt","r",stdin);
    int n,k;
    scanf("%d%d",&n,&k);
    memset(a,0,sizeof(a));
    for (int i=1;i<=n;i++)
    {
        scanf("%d",&a[i]);
        a[i]+=a[i-1];
    }
    double tmp=0.0;
    int i,j;
    for (i=1;i<=n-k+1;i++)
    {
        for (j=n;j>=i+k-1;j--)
        if ((double(a[j]-a[i-1])/double(j-i+1)-tmp)>eps)
        {
            tmp=double(a[j]-a[i-1])/double(j-i+1);
        }
    }
    printf("%.2lf",tmp);
}
