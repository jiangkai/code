#include <cstdio>

int clock[11]={110,166,221,276,332,387,443,498,553,609,664};

int main()
{
    //freopen("in.txt","r",stdin);
    int t;
    scanf("%d",&t);
    while(t--)
    {
        int h,m;
        int s,e;
        scanf("%d:%d",&h,&m);
        s=h*60+m;
        scanf("%d:%d",&h,&m);
        e=h*60+m;
        int start=0,end=0;
        for (int i=0;i<11;i++)
        {
            if (e > clock[i] )
                end=i;
        }
        for (int i=10;i>=0;i--)
        {
            if (s <= clock[i] )
                start=i-1;
        }
        //printf("s:%d,e:%d\n",start,end);
        printf("%d\n",end-start);
    }
}
