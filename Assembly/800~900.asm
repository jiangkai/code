.486
DATA	SEGMENT	USE16
INCOME	DW	100,200,300,400,500,600,700,800,900
A 		EQU	800
B 		EQU 900 
RESULT	DB	'result=',?,'$'
DATA	ENDS
CODE	SEGMENT USE16
		ASSUME CS:CODE,DS:DATA
BEG:	MOV	AX,DATA
		MOV	DS,AX
		LEA	BX,INCOME
		MOV	CX,9
		MOV	AL,0
XUN:	
LAST:	CMP WORD PTR [BX],A
		JNC NEXT				;IF>800					
		JMP TA
NEXT:	CMP WORD PTR [BX],B
		JC  AD					;IF<900
		JMP TA
AD:		INC AL
TA:		INC BX
		LOOP XUN
		ADD AL,30H
		MOV RESULT+7,AL
		MOV AH,9
		MOV DX,OFFSET RESULT
		INT 21H
		MOV AH,4CH
		INT 21H
CODE	ENDS
		END 	BEG