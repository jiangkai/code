#include <cstdio>
#include <cstring>
#define MAXN 25

int n,m,ans;
char map[MAXN][MAXN];
int vis[MAXN][MAXN];

void search(int r,int c)
{
    if (r<0||r>=n||c<0||c>=m||map[r][c]=='#'||vis[r][c])
        return;
    vis[r][c]=1;
    ++ans;
    search(r-1,c);search(r+1,c);
    search(r,c-1);search(r,c+1);
}

int main()
{
    scanf("%d%d",&m,&n);
    while (n||m)
    {
        int r,l;
        for (int i=0;i<n;i++)
        {
            scanf("%s",map[i]);
            for(int j=0;j<m;j++)
            {
                if(map[i][j]=='@')
                {
                    r=i;l=j;
                }
            }
        }
        memset(vis,0,sizeof(vis));
        ans=0;
        search(r,l);
        printf("%d\n",ans);
        scanf("%d%d",&m,&n);
    }
    return 0;
}
