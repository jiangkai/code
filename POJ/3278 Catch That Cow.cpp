#include <iostream>
#include <queue>
using namespace std;

int step[210000];
int vis[210000];
int n,k;

int main()
{
    queue<int> q;
    while (scanf("%d %d",&n,&k) == 2)
    {
        memset(step,0,sizeof(step));
        memset(vis,0,sizeof(vis));
        q.push(n);                          //将n放入队列
        step[n]++;                          //步数加一
        vis[n]++;
        while (!q.empty())
        {
            int cur=q.front();
            if (cur == k)  break;
            q.pop();
            if (!vis[cur-1] && cur-1>=0 && cur-1<=100000)
            {
                q.push(cur-1);
                step[cur-1]+=step[cur]+1;
                vis[cur-1]++;
            }
            if (!vis[cur+1] && cur+1>=0 && cur+1<=100000)
            {
                q.push(cur+1);
                step[cur+1]+=step[cur]+1;
                vis[cur+1]++;
            }
            if (!vis[cur*2] && cur*2>=0 && cur*2<=100000)
            {
                q.push(cur*2);
                step[cur*2]+=step[cur]+1;
                vis[cur*2]++;
            }
        }
        while(!q.empty())
            q.pop();
        printf("%d\n",step[k]-1);
    }
    return 0;
}
