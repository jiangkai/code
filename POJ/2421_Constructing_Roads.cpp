#include <cstdio>
#include <cstring>
#define MAXN 110
#define INF 10000

int N;
int sumweight;
int edge[MAXN][MAXN];
int lowcost[MAXN];

void prim()
{
    for (int i=0; i<N; i++)
    {
        lowcost[i]=edge[0][i];
    }
    lowcost[0] = -1;
    int j;

    for (int i=1; i<N; i++)
    {
        int min=INF;
        for (int k=0; k<N; k++)
        {
            if (lowcost[k] != -1 && lowcost[k] < min )
            {
                j=k;
                min=lowcost[k];
            }
        }
        lowcost[j]=-1;
        sumweight+=min;
        for (int i=0; i<N; i++)
        {
            if (edge[j][i]<lowcost[i])
                lowcost[i]=edge[j][i];
        }
    }
    printf("%d\n",sumweight);
}

int main()
{
    //freopen("in.txt","r",stdin);
    scanf("%d",&N);
    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++)
        {
            scanf("%d",&edge[i][j]);
        }
    int u,v,t;
    scanf("%d",&t);
    while (t--)
    {
        scanf("%d%d",&u,&v);
        edge[u-1][v-1]=edge[v-1][u-1]=0;
    }
    sumweight=0;
    prim();
    return 0;
}
