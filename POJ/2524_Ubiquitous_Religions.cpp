#include <cstdio>
#include <iostream>
using namespace std;

int parent[50005];

void init(int n)
{
    for (int i=0;i<=n;i++)
    {
        parent[i]=-1;
    }
}

int find(int x)
{
    int s;
    for ( s=x; parent[s]>=0; s=parent[s]);
    while (s != x)
    {
        int tmp;
        tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void merge(int r1,int r2)
{
    int u=find(r1),v=find(r2);
    int tmp=parent[u]+parent[v];
    if (parent[u] > parent[v] )
    {
        parent[u]=v;
        parent[v]=tmp;
    }
    else
    {
        parent[v]=u;
        parent[u]=tmp;
    }
}

int main()
{
    //freopen("in.txt","r",stdin);
    int n,m;
    int a,b;
    int kase=1;
    while ( scanf("%d%d",&n,&m) )
    {
        if ( n == 0 && m == 0 )  break;
        init(n);
        while( m-- )
        {
            scanf("%d%d",&a,&b);
            if ( find(a) != find(b) )
            {
                merge(a,b);
            }
        }
        int cnt=0;
        for (int i=1;i<=n;i++)
        {
            if (parent[i] < 0)
                cnt++;
        }
        printf("Case %d: %d\n",kase++,cnt);
    }
    return 0;
}
