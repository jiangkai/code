#include <cstdio>
#define MAXN 1005

struct node
{
    int c,p;
} map[MAXN][MAXN];

int n,m;
const int inf=0x7fffffff;
int dis[MAXN];
int p[MAXN];
int vis[MAXN];


void dijkstra(int s,int e)
{
    int i,j;
    for(i=1; i<=n; i++)
    {
        dis[i]=inf;
        p[i]=inf;
        vis[i]=0;
    }
    dis[s]=0;
    p[s]=0;
    for(i=1; i<=n; i++)
    {
        int min=inf,k;
        for(j=0; j<=n; j++)
        {
            if(!vis[j] && min>dis[j])
            {
                min=dis[j];
                k=j;
            }
        }
        vis[k]=1;
        for (j=1; j<=n; j++)
        {
            if (!vis[j] && map[k][j].c != inf && dis[j] > dis[k]+map[k][j].c) //!!not !vis[k]
            {
                dis[j]=dis[k]+map[k][j].c;
                p[j]=p[k]+map[k][j].p;
            }
            else if (dis[k]+map[k][j].c == dis[j])
            {
                if (map[k][j].p +p[k]<p[j])
                    p[j]=map[k][j].p+p[k];
            }
        }
    }
    printf("%d %d\n",dis[e],p[e]);
}

int main()
{
    freopen("in.txt","r",stdin);
    while (scanf("%d%d",&n,&m))
    {
        int a,b,cost,price;
        int start,end;
        if ( n==0 && m==0 )   break;
        for (int i=0; i<=n; i++)
            for (int j=0; j<=n; j++)
            {
                map[i][j].c=map[i][j].p=map[j][i].c=map[j][i].p=inf;
            }
        for (int i=0; i<m; i++)
        {
            scanf("%d%d%d%d",&a,&b,&cost,&price);
            if (cost < map[a][b].c )
            {
                map[a][b].c=map[b][a].c=cost;
                map[b][a].p=map[a][b].p=price;
            }
        }
        scanf("%d%d",&start,&end);
        dijkstra(start,end);
    }
    return 0;
}
