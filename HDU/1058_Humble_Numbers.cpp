/***********************
后面的丑数，必定是前面的一个丑数乘以2,3,5,7后比前一个大，而又最小的数.
应该是每次乘过一个因子，那么这个因子对应的数就向前走一步，下次计算的
时候就直接拿对应的最小数去乘以对应的因子，找到最小值.
状态转移方程：
F(n)=min(F(i)*2,F(j)*3,F(k)*5,F(m)*7)
                    (n>i,j,k,m)
特别的：
    i,j,k,m 只有在本项被选中后才移动
************************/
#include <cstdio>
#include <cstring>
#define max 5850

int min(int a,int b)
{
    return a<b?a:b;
}

int f[max];

int main()
{
    int a,b,c,d,n;
    a=b=c=d=1;
    f[1]=1;
    for (int i=2;i<=5842;i++)
    {
        f[i]=min(min(f[a]*2,f[b]*3),min(f[c]*5,f[d]*7));
        if (f[i]==f[a]*2)     a++;
        if (f[i]==f[b]*3)     b++;
        if (f[i]==f[c]*5)     c++;
        if (f[i]==f[d]*7)     d++;
    }
    while (scanf("%d",&n))
    {
        if (n== 0)  break;
        printf("The %d",n);
        if (n%100 !=11 && n%10 == 1)    printf("st");
        else if ( n % 100 !=12 && n%10 ==2)  printf("nd");
        else if (n % 100 != 13 && n % 10 ==3)   printf("rd");
        else printf("th");
        printf(" humble number is %d.\n", f[n]);
    }
    return 0;
}
