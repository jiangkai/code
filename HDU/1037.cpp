#include <iostream>
using namespace std;
int main()
{
    int t=3;
    int a;
    while (t--){
        cin>>a;
        if ( a<=168 ){
            cout<<"CRASH "<<a<<endl;
        }
        else cout<<"NO CRASH"<<endl;
    }
    return 0;
}
