#include <cstdio>
int main()
{
    int t,a,s,flag=1;
    scanf("%d",&s);
    while ( s-- )
    {
        scanf("%d",&t);
        int sum=0;
        while (t--)
        {
            scanf("%d",&a);
            sum+=a;
        }
        if (flag == 1)
        {
            printf("%d\n",sum);
            flag=0;
        }
        else printf("\n%d\n",sum);
    }
    return 0;
}
