#include <cstdio>
#include <iostream>
using namespace std;

int a[100];
int main()
{
    int t;
    int s;
    int kase=1;
    scanf("%d",&t);
    while ( t-- )
    {
        scanf("%d",&s);
        int sum=0;
        for (int i=0;i<s;i++)
        {
            scanf("%d",&a[i]);
            sum+=a[i];
        }
        double f=(1.0*a[0])/sum;
        printf("Case %d: %.6lf\n",kase++,f);
    }
    return 0;
}
