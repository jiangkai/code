/*****************************
   Edition 1
#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
using namespace std;
#define MAXN 1010

int k , ans;

long long Pow(long long x , long long y){
    long long tmp = x;
    for(long long i = 1 ; i <  y ; i++)
       x *= tmp;
    return x;
}

void solve(){
    int x , y , z;
    long long  left , right , mid , tmp;
    ans = 0;
    for(z = 2 ; z < 32 ; z++){
       for(x = 1 ; ; x++){
          if(Pow(x, z) >= k/2)
               break;
          left = x + 1;
          right = k;
          while(left <= right){
             mid = (left+right)/2;
             tmp = Pow(x,z)+Pow(mid,z)+mid*x*z;
             if(tmp == k){
                ans++;
                break;
             }
             else if(tmp > k|| tmp < 0)//注意这个地方数据类型的溢出
                right = mid-1;
             else
                left = mid+1;
          }
       }
    }
    printf("%d\n" , ans);
}

int main(){
    freopen("in.txt" , "r" , stdin);
    freopen("out.txt","w",stdout);
    while(scanf("%d" ,&k) !=EOF)
        solve();
    return 0;
}

*************************************/


/***********************************************
pow 函数返回的double
long long 表示
************************************************/
#include <cstdio>
#include <cstring>
#include <cmath>

long long Pow(long long x , long long y){
    long long tmp = x;
    for(long long i = 1 ; i <  y ; i++)
       x *= tmp;
    return x;
}


void solve(long long  x)
{

    int cnt=0;
    long long  temp = sqrt(x);
    //int  temp = sqrt(x);
    if(temp*temp==x)
        cnt += (temp-1)/2;

    for (int i=3;i<31;i++)
    {
        for (int j=1; ;j++)
        {
            long long u= Pow(j,i);
            //int u= Pow(j,i);
            if ( u> x/2 )   break;
            for (int k=j+1; ;k++)
            {
                long long v=Pow(k,i);
                //int v=Pow(k,i);
                if (u + v + k*i*j > x)  break;
                if ( u+ v +k*i*j == x)
                {
                    //printf("%d %d %d\n",i,j,k);
                    cnt++;
                    break;
                }
            }
        }
    }
    printf("%d\n",cnt);
}

int main()
{
    //freopen("in.txt","r",stdin);
    int k;
    while ( scanf("%d",&k) != EOF )
    {
        if (k == 0)
           break;
        solve(k);
    }

    return 0;
}
