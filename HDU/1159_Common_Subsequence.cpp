#include <cstdio>
#include <cstring>
#define MAXN 500

int c[2][MAXN];
char A[MAXN],B[MAXN];
int max(int a,int b)
{
    return a>b?a:b;
}

int lcs()
{
    int len1,len2;
    int tag=1;
    len1=strlen(A);
    len2=strlen(B);
    for (int i=0;i<=len1;i++)
        c[0][len1]=0;
    for (int i=0;i<=len2;i++)
        c[len2][0]=0;
    for (int i=1;i<=len1;i++)
    {
        for (int j=1;j<=len2;j++)
        {
          if(A[i-1] == B[j-1])
                c[tag][j] = c[1-tag][j-1] + 1;
            else
                c[tag][j] = max(c[1-tag][j],c[tag][j-1]);
        }
        tag = 1 - tag;
    }
    return c[1-tag][len2];
}

int main()
{
    while (scanf("%s%s",A,B) != EOF )
    {
        memset(c,0,sizeof(c));
        printf("%d\n",lcs());
    }
    return 0;
}
