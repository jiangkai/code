#include <cstdio>
int main ()
{
    int t;
    int Y,d,year;
    scanf("%d",&t);
    while (t--)
    {
        int cnt=0;
        scanf("%d %d",&year,&d);
        for (Y=year;;Y++)
        {
            if( (Y %4==0 && Y %100!=0) || Y%400 == 0 )
                cnt++;
            if ( cnt == d )
            {
                printf("%d\n",Y);
                break;
            }
        }
    }
    return 0;
}
