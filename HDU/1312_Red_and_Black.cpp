
#include<iostream>
#include<cstring>
#include<queue>
#include<stdio.h>
using namespace std;
int n,m,ans,vis[21][21];
char map[21][21];
int dir[4][2]={0,1,0,-1,1,0,-1,0};

struct node
{
    int x,y;
    node(int _x=0,int _y=0):x(_x),y(_y){};
};
queue <node> q;
node s;
void DFS()
{
    q.push(s);
    vis[s.x][s.y]=1;
    while(!q.empty())
    {
        node t=q.front();
        q.pop();
        for(int k=0;k<4;k++)
        {
            int x=t.x+dir[k][0];
            int y=t.y+dir[k][1];
            if(x>=0&&x<n&&y>=0&&y<m&&!vis[x][y]&&map[x][y]=='.')
            {
                vis[x][y]=1;
                ans++;
                q.push(node(x,y));
            }
        }
    }
    return ;
}
int main()
{
    freopen("in.txt","r",stdin);
    while(cin>>m>>n)
    {
        if(!m&&!n)break;
        for(int i=0;i<n;i++)
        {
            scanf("%s",&map[i]);
            for(int j=0;j<m;j++)
                if(map[i][j]=='@'){s.x=i,s.y=j;}
        }
        memset(vis,0,sizeof(vis));
        ans=1;
        DFS();
        printf("%d\n",ans);
    }
}
