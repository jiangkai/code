#include <cstdio>
#include <cstring>

char map[9][9];
int dx[]={1,-1,0,0},dy[]={0,0,1,-1};
int vis[9][9],dist[9][9];
int cnt=0;
int q[100];
void bfs(int i,int j,int n,int m)
{
    int front=0,rear=0;
    int u=i*m+j;
    vis[i][j]=1;dist[i][j]=0;
    q[rear++]=u;
    while(front<rear)
    {
        u=q[front++];
        i=u/m;j=u%m;
        for (int d=0;d<4;d++)
        {
            int nx=i+dx[d],ny=j+dy[d];
            if (nx>=0 && nx<n && ny>=0 && ny<m && map[nx][ny]!='X' && !vis[nx][ny])
            {
                int v=nx*m+ny;
                q[rear++] = v;
                vis[nx][ny]=1;
                dist[nx][ny]=dist[i][j]+1;
            }
        }
    }
}

int main ()
{
    freopen("in.txt","r",stdin);
    int n,m,t;
    while (scanf("%d %d %d",&n,&m,&t) && (n && m && t))
    {
        memset(map,0,sizeof(map));
        memset(vis,0,sizeof(vis));
        memset(dist,0,sizeof(dist));
        for (int i=0;i<n;i++)
            scanf("%s",map[i]);
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
            {
                if (map[i][j] == 'S')
                {
                    bfs(i,j,n,m);
                    break;
                }
            }
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
            {
                if (map[i][j] == 'D')
                {
                    if (dist[i][j] == t)
                        printf("Yes\n");
                    else
                        printf("No %d\n",dist[i][j]);
                    break;
                }
            }
    }
    return 0;
}
