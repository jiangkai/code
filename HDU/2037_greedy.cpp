#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

struct timex
{
    int start,end;
}S[105];

bool cmp(timex a,timex b)
{
    if (a.end == b.end)
        return a.start < b.start;
    else
        return a.end < b.end;
}

int main()
{
    int kase;
    while ( ~scanf("%d",&kase) )
    {
        if (kase == 0)  break;
        for ( int i=0; i<kase; i++ )
        {
            scanf("%d%d",&S[i].start,&S[i].end);
        }
        sort(S,S+kase,cmp);
        int count=1;
        int flag=0;
        for(int i=0;i<kase;i++)
        {
            if (S[i].start  >= S[flag].end)
            {
                count++;
                flag=i;
            }
        }
        printf("%d\n",count);
    }
    return 0;
}
