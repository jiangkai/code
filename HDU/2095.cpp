#include <cstdio>

int main()
{
    int t,s;
    while ( scanf("%d",&t)== 1 && t )
    {
        int cnt=0;
        for (int i=0;i<t;i++)
        {
            scanf("%d",&s);
            cnt^=s;
        }
        printf("%d\n",cnt);
    }
    return 0;
}
