#include <cstdio>
#include <algorithm>

int comp(const void*a,const void*b)
{
    return (*(int *)a - *(int *)b);
}
int main ()
{
    int t;
    int s[11];
    scanf("%d",&t);
    while (t--)
    {
        int n,flag;
        scanf("%d",&n);
        for (int i=0;i<n;i++)
            scanf("%d",&s[i]);
        qsort(s,n,sizeof(int),comp);
        printf("%d\n",s[1]);
    }
    return 0;
}
