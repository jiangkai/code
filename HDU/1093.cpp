#include <cstdio>
int main()
{
    int t,a,s;
    scanf("%d",&s);
    while ( s-- )
    {
        scanf("%d",&t);
        int sum=0;
        while (t--)
        {
            scanf("%d",&a);
            sum+=a;
        }
        printf("%d\n",sum);
    }
    return 0;
}
