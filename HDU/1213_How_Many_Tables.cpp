#include <cstdio>
int parent[1050];

int find (int x)
{
    int s;
    for (s=x;parent[s]>=0;s=parent[s]);
    while (x!=s)
    {
        int tmp;
        tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void Union(int x,int y)
{
    int r1=find(x),r2=find(y);
    int tmp=parent[r1]+parent[r2];
    if (parent[r1]<parent[r2])
    {
        parent[r2]=r1;
        parent[r1]=tmp;
    }
    else
    {
        parent[r1]=r2;
        parent[r2]=tmp;
    }
}

int main()
{
    int t;
    int n,m;
    int u,v;
    scanf("%d",&t);
    while (t--)
    {
        scanf("%d%d",&n,&m);
        for (int i=1;i<=n;i++)
            parent[i]=-1;
        for (int i=1;i<=m;i++)
        {
            scanf("%d%d",&u,&v);
            if (find(u)!=find(v))
                Union(u,v);
        }
        int sum=0;
        for (int i=1;i<=n;i++)
        {
            if (parent[i]<0)
                sum++;
        }
        printf("%d\n",sum);
    }
    return 0;
}
