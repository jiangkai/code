#include <cstdio>
#include <cstring>

int main()
{
    int t,set=1;
    int a[60];
    while ( scanf("%d",&t) == 1 && t != 0 )
    {
        int sum=0,avg,step=0;
        for (int i=0;i<t;i++)
        {
            scanf("%d",&a[i]);
            sum+=a[i];
        }
        avg=sum/t;
        for (int i=0;i<t;i++)
        {
            if (a[i]>avg)
            {
                step+=(a[i]-avg);
            }
        }
        printf("Set #%d\n",set++);
        printf("The minimum number of moves is %d.\n\n",step);
    }
    return 0;
}
