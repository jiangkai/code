#include <cstdio>
int N,M;
int parent[1010];

void init()
{
    for (int i=1;i<=N;i++)
        parent[i]=-1;
}

int find (int x)
{
    int s;
    for (s=x;parent[s]>=0;s=parent[s]);
    while (s!=x)
    {
        int tmp=parent[x];
        parent[x]=s;
        x=tmp;
    }
    return s;
}

void Union(int R1,int R2)
{
    int r1=find(R1),r2=find(R2);
    int tmp=parent[r1]+parent[r2];
    if (parent[r1]>parent[r2])
    {
        parent[r1]=r2;
        parent[r2]=tmp;
    }
    else
    {
        parent[r2]=r1;
        parent[r1]=tmp;
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    int x,y;
    while (scanf("%d",&N)==1)
    {
        int count=0;
        if ( N == 0 )   break;
        scanf("%d",&M);
        init();
        while (M--)
        {
            scanf("%d%d",&x,&y);
            if (find(x)!= find(y))
            {
                Union(x,y);
            }
        }
        for (int i=1;i<=N;i++)
        {
            if (parent[i]<0)
                count++;
        }
        printf("%d\n",count-1);
    }
}
