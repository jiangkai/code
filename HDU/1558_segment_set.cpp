#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define MAX 1002
using namespace std;

struct point
{
    double x,y;
};
struct segment
{
    point start,ter;
}seg [MAX];

int parent[MAX];
const double eps=1e-8;

double min(double a, double b)
{
    return a < b ? a : b;
}

double max(double a, double b)
{
    return a > b ? a : b;
}
bool inter(point a, point b, point c, point d)
{
    if ( min(a.x, b.x) > max(c.x, d.x) ||min(a.y, b.y) > max(c.y, d.y) ||
            min(c.x, d.x) > max(a.x, b.x) ||min(c.y, d.y) > max(a.y, b.y) )
        return 0;
    double h, i, j, k;
    h = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    i = (b.x - a.x) * (d.y - a.y) - (b.y - a.y) * (d.x - a.x);
    j = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x);
    k = (d.x - c.x) * (b.y - c.y) - (d.y - c.y) * (b.x - c.x);
    return h * i <= eps && j * k <= eps;
}

void init()
{
    for (int i=0; i<MAX; i++)
        parent[i]=-1;
}

int find(int u)
{
    int s;
    for (s=u; parent[s]>=0; s=parent[s]);
    while ( u!=s )
    {
        int tmp;
        tmp=parent[u];
        parent[u]=s;
        u=tmp;
    }
    return s;
}
void merge(int a,int b)
{
    int u=find(a),v=find(b);
    int tmp=parent[u]+parent[v];
    if (parent[u] < parent[v])
    {
        parent[v]=u;
        parent[u]=tmp;
    }
    else
    {
        parent[u]=v;
        parent[v]=tmp;
    }
}

int main()
{
    freopen("in.txt","r",stdin);
    int t,command;
    int kase;
    int num;
    char a;
    scanf("%d",&t);
    while(t--)
    {
        num=1;
        scanf("%d",&command);
        init();
        while (command--)
        {
            cin>>a;
            if ( a == 'P')
            {
                scanf("%lf%lf%lf%lf",&seg[num].start.x,&seg[num].start.y,&seg[num].ter.x,&seg[num].ter.y);
                for (int i=1; i<num; i++)
                {
                    if (inter(seg[i].start,seg[i].ter,seg[num].start,seg[num].ter))
                        if ( find(i) != find (num) )  ///!!!!!!!!!!!!!*****
                            merge(i,num);
                }
                num++;
            }
            if ( a == 'Q')
            {
                cin>>kase;
                kase=find(kase);
                printf("%d\n",-parent[kase]);
            }
        }
        if(t)                           ///****!!!!***!!!****!!!!****!!
            printf("\n");
    }
    return 0;
}
