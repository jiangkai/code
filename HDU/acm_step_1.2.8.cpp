#include <cstdio>
#include <cstring>

int main()
{
    freopen("in.txt","r",stdin);
    int t;
    char id[18];
    scanf("%d",&t);
    while ( t-- )
    {
        memset(id,0,sizeof(id));
        scanf("%s",id);
        printf("He/She is from ");
        switch((id[0]-'0')*10+(id[1]-'0'))
        {
            case 33:printf("Zhejiang");break;
            case 11:printf("Beijing");break;
            case 71:printf("Taiwan");break;
            case 81:printf("Hong Kong");break;
            case 82:printf("Macao");break;
            case 54:printf("Tibet");break;
            case 21:printf("Liaoning");break;
            case 31:printf("Shanghai");break;
        }
        printf(",and his/her birthday is on ");
        printf("%d%d,%d%d,%d%d%d%d",id[10]-'0',id[11]-'0',id[12]-'0',id[13]-'0',id[6]-'0',id[7]-'0',id[8]-'0',id[9]-'0');
        printf(" based on the table.\n");
    }
    return 0;
}
