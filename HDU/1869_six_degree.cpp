#include <cstdio>
#include <iostream>
using namespace std;
const int inf=0xfffffff;

int map[105][105];
int main()
{
    freopen("in.txt","r",stdin);
    int t,n;
    while( scanf("%d%d",&t,&n) != EOF )
    {
        int flag=1;
        int a,b;
        int i,j,k;
        for (i=0;i<t;i++)
            for (j=0;j<t;j++)
            {
                map[i][j]=inf;
            }
        for (i=0;i<n;i++)
        {
            scanf("%d%d",&a,&b);
            map[a][b]=map[b][a]=1;
        }
        for(k=0;k<t;k++)
            for (i=0;i<t;i++)
                for (j=0;j<t;j++)
                {
                    if (map[i][k]!=inf && map[k][j]!= inf && map[i][j] > map[i][k]+map[k][j])
                        map[i][j]=map[i][k]+map[k][j];
                }
        for (i=0;flag && i<t;i++)
            for (j=0;j<t;j++)
            {
                if (map[i][j]>7)
                {
                    flag=0;
                    break;
                }
            }
        if (flag)
            printf("Yes\n");
        else
            printf("No\n");
    }
    return 0;
}
