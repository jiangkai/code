#include <cstdio>
int main()
{
    int t,a;
    while (scanf("%d",&t) == 1) // !=EOF
    {
        int sum=0;
        while (t--)
        {
            scanf("%d",&a);
            sum+=a;
        }
        printf("%d\n",sum);
    }
    return 0;
}
