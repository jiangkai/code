#include <iostream>
using namespace std;

class Base{
private:
    int a;
public:
    Base (int x)
    {
        a=x;
    }
    void show()
    {
        cout<<"Base a="<<a<<endl;
    }
};

class Derived: public Base{
private:
    int b;
public:
    Derived (int i):Base(i+1)
    {
        b=i;
    }
    void show()
    {
        cout<<"Deriver b="<<b<<endl;
    }
};

int main ()
{
    Base b(5),*pb;
    b.show();
    Derived d(0);
    pb=&d;
    pb->show();
}
