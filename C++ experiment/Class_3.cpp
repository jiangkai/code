#include <iostream>
#include <iomanip>
using namespace std;
class Date{
private:
    int year;
    int month;
    int day;
public:
    Date(int y=1990,int m=1,int d=1)
    {
        year=y;month=m;day=d;
    }
    void setYear(int y);
    void setMonth(int m);
    void setDay(int d);
    void Date::setDate(int yr,int mo,int dy)
    {
        setYear(yr);setMonth(mo);setDay(dy);
    }
    int getYeat()
    {
        return year;
    }
    int getMonth()
    {
        return month;
    }
    int getDay()
    {
        return day;
    }
    void print();
    bool isLeapYear();
    int monthDays();
    void nextday();
};

void Date::setMonth(int m)
{
    month=m;
}

void Date::setYear(int y)
{
    year=y;
}

void Date::setDay(int d)
{
    if (month==2 && isLeapYear())
        day=(d>=1 && d<=29) ? d : 1;
    else
        day=(d>=1 && d<=monthDays()) ? d :1;
}

bool Date::isLeapYear()
{
    if (year%400==0 || (year%4 ==0 && year%100 !=0))
        return true;
    else
        return false;
}

int Date::monthDays()
{
    const int days[12]={31,28,31,30,31,30,31,31,30,31,30,31};
    return month == 2&& isLeapYear() ? 29: days[month-1];
}


void Date::print()
{
    cout<<year<<"--"<<month<<"--"<<day<<endl;
}

void Date::nextday()
{
    day++;
    if (day>monthDays())
    {
        day=1;month++;
        if (month>12)
        {
            month=1;year++;
        }
    }
}

int main()
{
    int year,month,day,i,n;
    Date d;
    d.print();
    printf("input yyyy-mm-dd:");
    scanf("%d-%d-%d",&year,&month,&day);
    printf("after:");
    scanf("%d",&n);
    d.setDate(year,month,day);
    d.print();
    for (i=0;i<n;i++)
    {
        d.nextday();
    }
    d.print();
    return 0;
}
