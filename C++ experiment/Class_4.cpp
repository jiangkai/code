#include <iostream>
using namespace std;

class Increment{
public:
    Increment(int c=0,int i=1);
    void addIncrement()
    {
        count+=increment;
    }
    void print() const;
private:
    int count;
    const int increment;
};

Increment::Increment(int c,int i):increment(i)
{
    count=c;
}

void Increment::print() const
{
    cout<<"count="<<count<<",increment="<<increment<<endl;
}

int main ()
{
    Increment value(10,5);
    cout<<"before incrementing:";
    value.print();
    for (int j=1;j<=3;j++)
    {
        value.addIncrement();
        cout<<"after increment "<<j<<":";
        value.print();
    }
    return 0;
}
