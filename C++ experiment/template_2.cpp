#include <cstdio>
#include <iostream>
using namespace std;

template <class T>
class Sample
{
    T n;
public:
    Sample(T i){
        n=i;
    }
    bool operator==(Sample<T> &a){
        return a.n == n;
    }
};

int main()
{
    Sample <int> s1(2),s2(3);
    cout<<"s1与s2的数据成员"<<((s1==s2)?"相等":"不相等")<<endl;
    Sample <double> s3(2.5),s4(2.5);
    cout<<"s3与s4的数据成员"<<((s3==s4)?"相等":"不相等")<<endl;
    return 0;
}
