#include <iostream>

using namespace std;

class vehicle{
private:
	int MaxSpeed;
	int Weight;
public:
	vehicle()
	{
		Weight=0;
		MaxSpeed=0;
	}
	~vehicle(){}
	void Run()
	{
		cout<<"正在行驶中！"<<endl;
	}
	void Stop()
	{
		cout<<"停车！"<<endl;
	}
};

class bicycle: virtual public vehicle{
protected:
	int Height,B_yWeight;
public:
	bicycle(int h,int B_yW):vehicle()
	{
		Height=h;B_yWeight=B_yW;
	}
	~bicycle(){};
	void bY_show()
	{
		cout<<"自行车的重量"<<B_yWeight;
		cout<<"自行车的高度"<<Height<<endl;
	}
};

class motorcar : virtual public vehicle{
protected:
	int SeatNum,M_carMaxSpeed;
public:
	motorcar(int MC_Sp,int s): vehicle() {SeatNum=s;M_carMaxSpeed=MC_Sp;}
	~motorcar() {};
	void mc_show()
	{
		cout<<"汽车的最高时速"<<M_carMaxSpeed;
		cout<<"汽车的载客量"<<SeatNum<<endl;
	}
};

class motorcycle: public bicycle,public motorcar{
public:
    motorcycle(int mH,int mS,int BW,int BS):bicycle(mH,BW),motorcar(BS,mS){}
    ~motorcycle(){};
    void mc_show()
    {
        cout<<"摩托车的高度"<<Height;
        cout<<"摩托车的载客量"<<SeatNum;
        cout<<"摩托车的重量"<<B_yWeight;
        cout<<"摩托车的最高时速"<<M_carMaxSpeed<<endl;
    }
};

int main ()
{
    int drive;
    motorcycle mcy(22,3,30,120);
    mcy.mc_show();
    bicycle by(24,30);
    by.bY_show();
    motorcar mcar(240,19);
    mcar.mc_show();
    while (drive)
    {
        cout<<"\n开车请输入1停车输入0"<<endl;
        cin>>drive;
        if (drive==1) mcy.Run();
        if (drive==0) mcy.Stop();
    }
    return 0;
}
