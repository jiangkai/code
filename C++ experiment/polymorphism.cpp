#include <cstdio>
#include <cmath>
#include <iostream>
using namespace std;

const double pi=4.0*atan(1.0);

class container{
protected:
	double radius;
public:
	container(double radius){
		container::radius=radius;
	}
	virtual double surface_area()=0;
	virtual double volume()=0;
};

class cube:public container{
public:
	cube(double length):container(length){
		radius=length;
	}
	virtual double surface_area(){
		return radius*radius*6.0;
	}
	virtual double volume(){
		return radius*radius*radius;
	}
};

class sphere:public container{
public:
	sphere(double r):container(r){
		radius=r;
	}
	virtual double surface_area(){
		return 4.0*pi*radius*radius;
	}
	virtual double volume (){
		return (double(4.0/3.0))*pi*radius*radius*radius;
	}
};

class cylinder:public container{
private:
	double height;
public:
	cylinder(double r,double h):container(r){
		height=h;
	}
	virtual double surface_area(){
		return 2.0*pi*height*radius+2.0*pi*radius*radius;
	}
	virtual double volume(){
		return 2.0*pi*radius*radius*height;
	}
};

int main()
{
	container *p;
	cube obj1(10);
	sphere obj2(6);
	cylinder obj3(4,5);
	p=&obj1;
	cout<<"output result:"<<endl;
	cout<<"cube's surface area:"<<p->surface_area()<<endl;
	cout<<"cube's volume:"<<p->volume()<<endl;
	p=&obj2;
	cout<<"sphere's surface area:"<<p->surface_area()<<endl;
	cout<<"sphere's volume:"<<p->volume()<<endl;
	p=&obj3;
	cout<<"cylinder's surface area:"<<p->surface_area()<<endl;
	cout<<"cylinder's volume:"<<p->volume()<<endl;
	return 0;
}
