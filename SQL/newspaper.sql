drop database if exists `joyfulmo_newspaper`;
create database  `joyfulmo_newspaper` default character set = 'utf8';
use `joyfulmo_newspaper`;
create table `报纸编码表`(
	`报纸编号` char(7) NOT NULL ,
	`报纸名称` char(10),
	`单价` float,
	primary key (`报纸编号`)
	);
create table `顾客订阅表`(
	`客户标号` char(7) NOT NULL,
	`报纸编号` char(7) ,
	`订阅份数` smallint default 1,
	primary key (`客户标号`,`报纸编号`),
	foreign key (`报纸编号`) references `报纸编码表`(`报纸编号`)	
);
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0001', '人民日报', '12.5');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0002', '解放军报', '14.6');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0003', '青年报', '13.5');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0004', '扬子晚报', '12.5');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0005', '金陵晚报', '19.4');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0006', '环球时报', '10.2');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0007', 'New York Post', '22.5');
INSERT INTO `joyfulmo_newspaper`.`报纸编码表` (`报纸编号`, `报纸名称`, `单价`) VALUES ('0008', 'Times', '32.6');

INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1101', '0002', 1);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1101', '0005', 5);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1101', '0003', 2);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1101', '0004', 1);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1101', '0008', 4);

INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1102', '0005', 4);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1102', '0001', 8);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1102', '0002', 1);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1102', '0006', 7);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1102', '0003', 1);

INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1103', '0001', 10);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1103', '0002', 1);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1103', '0003', 7);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1103', '0004', 8);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1103', '0006', 3);

INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1104', '0006', 2);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1104', '0002', 5);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1104', '0003', 2);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1104', '0005', 10);
INSERT INTO `joyfulmo_newspaper`.`顾客订阅表` (`客户标号`, `报纸编号`, `订阅份数`) VALUES ('1104', '0007', 4);


