drop database if exists jxgl;
create database jxgl;
use jxgl;
create table student
	(
		sno CHAR(7) NOT NULL ,
		sname varchar(16),
		ssex CHAR(10) DEFAULT 'male' CHECK (ssex='male'or ssex='female'),
		sage smallint CHECK(sage <= 45 and sage >= 12),
		sdept CHAR(2),
		primary key (sno)
		)engine=InnoDB;
create table course
	(
		cno CHAR(2) NOT NULL,
		cname varchar(20),
		cpno CHAR(2),
		credit smallint,
		primary key (cno),
		foreign key (cpno) references course(cno)
		)engine=InnoDB;
create table sc
	(
		sno CHAR(7) NOT NULL,
		cno CHAR(2) NOT NULL,
		grade smallint NULL CHECK (grade is NULL or (grade between 0 and 100)),
		primary key (sno,cno),
		foreign key (sno) references student(sno),
		foreign key (cno) references course(cno)
		)engine =InnoDB;