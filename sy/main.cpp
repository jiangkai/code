#include <iostream>
using namespace std;
class Rectangle {
protected:
	double length;
	double width;
public:

	Rectangle(double l,double w)
	{
		length=l;
		width=w;
	}
	~Rectangle() {};
	void area ()
	{
		cout<<"The area is:"<<length*width<<endl;
	}
};

class Rectangular : public Rectangle{
private:
	double height;
public:
	Rectangular(double l,double w,double h):Rectangle(l,w)
	{
		height=h;
	}
	~Rectangular () {};
	void volume ()
	{
		cout<<"The volume is:"<<height*length*width<<endl;
	}
};

int main ()
{
	Rectangle r2(5,6);
	Rectangular r3(5,6,7);
	r2.area();
	r3.volume();
	return 0;
}
