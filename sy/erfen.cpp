#include <iostream>
#include <cstdio>
#include <cmath>
const double eps=1e-8;

double f(double x)
{
    return 1+x-x*x*x;
}

int main()
{
    double a,b,e;
    printf("the function is f(x)=1+x-x*x*x\n");
    printf("input a b e:");
    scanf("%lf%lf%lf",&a,&b,&e);
    e=fabs(e);
    if (fabs(f(a))<=e)
        printf("solution: %lg\n",a);
    else

        if (fabs(f(b))<=e)
        {
            printf("solution: %lg\n",b);
        }
        else if (f(a)*f(b)>0)
           {
               printf("f(%lf)*f(%lf)>0",a,b);
           }
            else
            {
            while (fabs(b-a)>e)
            {
                double c=(a+b)/2.0;
                if (f(a)*f(c)<0)
                    b=c;
                else
                    a=c;
            }

            printf("solution :%lg\n",(a+b)/2.0);
            }

}
